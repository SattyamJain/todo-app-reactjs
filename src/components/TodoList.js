import React, { Component } from "react";
import TodoItem from "./TodoItem";

class TodoList extends Component {
  render() {
    const { items, clearList, handleDelete, handleEdit } = this.props;
    return (
      <div>
        <ul className="list-group">
          <h3 className="text-center">Todo Item List</h3>
          {items.map(item => {
            return (
              <TodoItem
                key={item.id}
                title={item.title}
                handleDelete={() => handleDelete(item.id)}
                handleEdit={() => handleEdit(item.id)}
              />
            );
          })}
          <br />
          <button type="submit" className="btn btn-danger" onClick={clearList}>
            Delete List
          </button>
        </ul>
      </div>
    );
  }
}

export default TodoList;
