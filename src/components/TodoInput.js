import React, { Component } from "react";

class TodoInput extends Component {
  render() {
    const { item, handleChange, handleSubmit, editItem } = this.props;
    return (
      <div>
        <h4 className="text-center">Input Data</h4>
        <br />
        <form onSubmit={handleSubmit}>
          <div className="input-group">
            <input
              type="text"
              placeholder="Enter the data"
              className="form-control"
              value={item}
              onChange={handleChange}
            />
            <button
              type="submit"
              className={editItem ? "btn btn-success" : "btn btn-primary"}
            >
              {editItem ? "Edit The Item" : "Add The Item"}
            </button>
          </div>
        </form>
      </div>
    );
  }
}

export default TodoInput;
