import React from "react";
import TodoInput from "./components/TodoInput";
import TodoList from "./components/TodoList";

import "bootstrap/dist/css/bootstrap.min.css";
import uuid from "uuid";

class App extends React.Component {
  state = {
    items: [],
    id: uuid,
    item: "",
    editItem: false
  };

  handleChange = event => {
    this.setState({
      item: event.target.value
    });
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.state.item === "") {
      alert("Enter an item");
      return;
    }
    const newItem = {
      id: this.state.id,
      title: this.state.item
    };

    if (this.state.items.find(item => item.title === newItem.title)) {
      alert("Item Already Exists");
      return;
    }

    const updatedItems = [...this.state.items, newItem];

    this.setState({
      items: updatedItems,
      item: "",
      id: uuid(),
      editItem: false
    });
  };

  handleDelete = id => {
    const filteredItems = this.state.items.filter(item => item.id !== id);
    this.setState({
      items: filteredItems
    });
  };

  handleEdit = id => {
    const filteredItems = this.state.items.filter(item => item.id !== id);

    const selectedItem = this.state.items.find(item => item.id === id);
    this.setState({
      items: filteredItems,
      item: selectedItem.title,
      editItem: true,
      id: id
    });
  };

  clearList = () => {
    this.setState({
      items: []
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-6 mx-auto col-md-8 mt-4">
            <h3 className="text-center">Todo React App</h3>
            <br />
            <br />
            <TodoInput
              item={this.state.item}
              handleChange={this.handleChange}
              handleSubmit={this.handleSubmit}
              editItem={this.state.editItem}
            />
            <br />
            <TodoList
              items={this.state.items}
              clearList={this.clearList}
              handleDelete={this.handleDelete}
              handleEdit={this.handleEdit}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
